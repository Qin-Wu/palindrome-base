package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindromeRegular( ) {
		String testWord="sos";
		assertTrue("This is not a palindrome word!", Palindrome.isPalindrome(testWord));
	}

	@Test
	public void testIsPalindromeNegative( ) {
		String testWord="String";
		assertFalse("This is a palindrome word!", Palindrome.isPalindrome(testWord));
	}
	
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		String testWord="x";
		assertTrue("This is not a palindrome word!", Palindrome.isPalindrome(testWord));
	}
	
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		String testWord="so";
		assertFalse("This is a palindrome word!", Palindrome.isPalindrome(testWord));	
	}		
}
